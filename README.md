ansible-role-users
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-users/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-users/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Manages deploy users and SSH keys on Debian systems.

Requirements
------------
None

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure dev system(s)
  hosts: all
  roles:
    - { role: ansible-role-users }
```

License
-------

BSD
