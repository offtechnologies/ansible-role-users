import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture
def get_vars(host):
    defaults_files = "file=../../defaults/main.yml name=role_defaults"
    vars_files = "file=../../vars/main.yml name=role_vars"

    ansible_vars = host.ansible(
        "include_vars",
        defaults_files)["ansible_facts"]["role_defaults"]

    ansible_vars.update(host.ansible(
        "include_vars",
        vars_files)["ansible_facts"]["role_vars"])

    return ansible_vars


@pytest.mark.parametrize('user', [
  ("jdoe"),
  ("ci")
])
def test_users(host, user, get_vars):
    user = host.user(user)

    assert user.exists
    assert user.shell == '/bin/bash'


@pytest.mark.parametrize('file', [
  ("/home/jdoe/.ssh/authorized_keys"),
  ("/home/ci/.ssh/authorized_keys")
])
def test_authorized_keys(host, file, get_vars):
    file = host.file(file)

    assert file.exists
